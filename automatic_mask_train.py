import argparse
import torch
from osrt.model import OSRT
import time
import matplotlib.pyplot as plt
import numpy as np
import cv2


def show_anns(masks):
    ax = plt.gca()
    ax.set_autoscale_on(False)    
    img = np.ones((masks['segmentations'][0].shape[0], masks['segmentations'][0].shape[1], 4))
    img[:,:,3] = 0
    for seg in masks['segmentations']:
        m = seg
        color_mask = np.concatenate([np.random.random(3), [0.95]])
        img[m] = color_mask
    ax.imshow(img)

def show_points(coords, ax, marker_size=100):
    ax.scatter(coords[:, 0], coords[:, 1], color='#2ca02c', marker='.', s=marker_size)
    

if __name__ == '__main__':
    # Arguments
    parser = argparse.ArgumentParser(
        description='Test Segment Anything Auto Mask simplified implementation'
    )
    parser.add_argument('--model', default='vit_h', type=str, help='Model to use')
    parser.add_argument('--path_model', default='.', type=str, help='Path to the model')
    parser.add_argument('--visualize', default=False, type=bool, help='Wether to visualize or no the segments')

    args = parser.parse_args()
    device = "cuda"

    model_type = args.model
    if args.model == 'vit_h':
        checkpoint = args.path_model + '/sam_vit_h_4b8939.pth'
    elif args.model == 'vit_b':
        checkpoint = args.path_model + '/sam_vit_b_01ec64.pth'
    else:
        model_type = 'vit_l'
        checkpoint = args.path_model + '/sam_vit_l_0b3195.pth'

    ycb_path = "/home/achapin/Documents/Datasets/YCB_Video_Dataset/"
    images_path = []
    with open(ycb_path + "image_sets/train.txt", 'r') as f:
        for line in f.readlines():
            line = line.strip()
            images_path.append(ycb_path + 'data/' + line + "-color.png")

    import random
    random.shuffle(images_path)
    cfg={}
    cfg['encoder'] = 'sam'
    cfg['decoder'] = 'slot_mixer'
    cfg['encoder_kwargs'] = {
        'points_per_side': 32,
        'box_nms_thresh': 0.7,
        'stability_score_thresh': 0.9,
        'pred_iou_thresh': 0.88,
        'sam_model': model_type, 
        'sam_path': checkpoint,
        'points_per_batch': 12
    }
    cfg['decoder_kwargs'] = {
        'pos_start_octave': -5,
    }
    model = OSRT(cfg)#FeatureMasking(points_per_side=12, box_nms_thresh=0.7, stability_score_thresh= 0.9, pred_iou_thresh=0.88, points_per_batch=64)
    model.to(device, non_blocking=True)

    """num_encoder_params = sum(p.numel() for p in model.encoder.parameters())
    num_decoder_params = sum(p.numel() for p in model.decoder.parameters())

    print('Number of parameters:')
    print(f'\tEncoder: {num_encoder_params}')

    num_mask_encoder_params = sum(p.numel() for p in model.encoder.mask_generator.parameters())
    num_vit_params = sum(p.numel() for p in model.encoder.mask_generator.image_encoder.parameters())
    num_mask_params = sum(p.numel() for p in model.encoder.mask_generator.mask_decoder.parameters())
    num_prompt_params = sum(p.numel() for p in model.encoder.mask_generator.prompt_encoder.parameters())
    num_slotatt_params = sum(p.numel() for p in model.encoder.slot_attention.parameters())
    print(f'\t\tMask Encoder: {num_mask_encoder_params}.')
    print(f'\t\t\tVit Encoder: {num_vit_params}.')
    print(f'\t\t\tMask Decoder: {num_mask_params}.')
    print(f'\t\t\tPrompt Encoder: {num_prompt_params}.')
    print(f'\t\tSlot Attention: {num_slotatt_params}.')
    print(f'\tDecoder: {num_decoder_params}')"""

    images= []
    from torchvision import transforms
    transform = transforms.ToTensor()
    for j in range(2):
        image = images_path[j]
        img = cv2.imread(image)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        images.append(transform(img).unsqueeze(0))
        #images.append(np.expand_dims(img, axis=0))
    #images_np = np.array(images)
    images_t = torch.stack(images).to(device)
    print(f"Shape image {images_t.shape}")

    images_t = images_t.permute(0, 1, 3, 4, 2)

    h, w, c = images_t[0][0].shape
    #print(f"Begin shape {images_np.shape}")
    points = model.encoder.mask_generator.points_grid
    new_points= []
    for val in points:
        x, y = val[0], val[1]
        x *= w
        y *= h
        new_points.append([x, y])
    new_points = np.array(new_points)
        
    start = time.time()
    # TODO : set ray and camera directions
    #with torch.no_grad():
    with torch.cuda.amp.autocast(): 
        masks = model.encoder.mask_generator(images_t, (h, w), None, None, extract_embeddings=False)
    end = time.time()
    print(f"Inference time : {int((end-start) * 1000)}ms")
    
    if args.visualize:
        for j in range(10):
            image = images_path[j]
            img = cv2.imread(image)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            plt.figure(figsize=(15,15))
            plt.imshow(img)
            show_anns(masks[j][0]) # show masks 
            show_points(new_points, plt.gca()) # show points
            #plt.axis('off')
            plt.show()