import torch
from segment_anything import sam_model_registry, SamAutomaticMaskGenerator
import matplotlib.pyplot as plt
import numpy as np
import cv2
import time

def show_anns(anns):
    if len(anns) == 0:
        return
    sorted_anns = sorted(anns, key=(lambda x: x['area']), reverse=True)
    ax = plt.gca()
    ax.set_autoscale_on(False)

    img = np.ones((sorted_anns[0]['segmentation'].shape[0], sorted_anns[0]['segmentation'].shape[1], 4))
    img[:,:,3] = 0
    for ann in sorted_anns:
        m = ann['segmentation']
        color_mask = np.concatenate([np.random.random(3), [0.95]])
        img[m] = color_mask
    ax.imshow(img)



model_type = "vit_t"
sam_checkpoint = "./mobile_sam.pt"

device = "cuda" if torch.cuda.is_available() else "cpu"

mobile_sam = sam_model_registry[model_type](checkpoint=sam_checkpoint)
mobile_sam.to(device=device)
mobile_sam.eval()

mask_generator = SamAutomaticMaskGenerator(mobile_sam, points_per_side=16, points_per_batch= 12)
ycb_path = "/home/achapin/Documents/Datasets/YCB_Video_Dataset/"
images_path = []
with open(ycb_path + "image_sets/train.txt", 'r') as f:
    for line in f.readlines():
        line = line.strip()
        images_path.append(ycb_path + 'data/' + line + "-color.png")

import random
#random.shuffle(images_path)

images= []
from torchvision import transforms
transform = transforms.ToTensor()
for j in range(20):
    image = images_path[j]
    img = cv2.imread(image)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    images.append(transform(img).unsqueeze(0))    
images_t = torch.stack(images).to(device)

start = time.time()
masks = mask_generator.generate(images_t)
end = time.time()
print(f"Inference time : {int((end-start) * 1000)}ms")
plt.figure(figsize=(15,15))
plt.imshow(img)
show_anns(masks) # show masks 
plt.axis('off')
plt.show()