from typing import Any
from lightning.pytorch.utilities.types import STEP_OUTPUT
from torch import nn
import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.optim.lr_scheduler import LambdaLR

import numpy as np

from osrt.encoder import OSRTEncoder, ImprovedSRTEncoder, FeatureMasking
from osrt.decoder import SlotMixerDecoder, SpatialBroadcastDecoder, ImprovedSRTDecoder
from osrt.layers import SlotAttention, TransformerSlotAttention, Encoder, Decoder
import osrt.layers as layers
from osrt.utils.common import mse2psnr, compute_adjusted_rand_index

import lightning as pl


class OSRT(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        encoder_type = cfg['encoder']
        decoder_type = cfg['decoder']

        layers.__USE_DEFAULT_INIT__ = cfg.get('use_default_init', False)

        if encoder_type == 'srt':
            self.encoder = ImprovedSRTEncoder(**cfg['encoder_kwargs'])
        elif encoder_type == 'osrt':
            self.encoder = OSRTEncoder(**cfg['encoder_kwargs'])
        elif encoder_type == 'sam':
            self.encoder = FeatureMasking(**cfg['encoder_kwargs'])
        else:
            raise ValueError(f'Unknown encoder type: {encoder_type}')


        if decoder_type == 'spatial_broadcast':
            self.decoder = SpatialBroadcastDecoder(**cfg['decoder_kwargs'])
        elif decoder_type == 'srt':
            self.decoder = ImprovedSRTDecoder(**cfg['decoder_kwargs'])
        elif decoder_type == 'slot_mixer':
            self.decoder = SlotMixerDecoder(**cfg['decoder_kwargs'])
        else:
            raise ValueError(f'Unknown decoder type: {decoder_type}')


class LitSlotAttentionAutoEncoder(pl.LightningModule):
    """
    Slot Attention as introduced by Locatello et al. but with the AutoEncoder part to extract image embeddings.

    Implementation inspired from official repo : https://github.com/google-research/google-research/blob/master/slot_attention/model.py
    """

    def __init__(self, resolution, num_slots, cfg):
        """Builds the Slot Attention-based auto-encoder.

        Args:
        resolution: Tuple of integers specifying width and height of input image.
        num_slots: Number of slots in Slot Attention.
        """
        super().__init__()
        self.resolution = resolution
        self.num_slots = num_slots
        self.num_iterations = cfg["model"]["iters"]

        self.criterion = nn.MSELoss()

        self.encoder = Encoder()
        self.decoder = Decoder()

        self.peak_it = cfg["training"]["warmup_it"]
        self.peak_lr = 1e-4
        self.decay_rate = 0.16
        self.decay_it = cfg["training"]["decay_it"]

        model_type = cfg['model']['model_type']
        if model_type == 'sa':
            self.slot_attention = SlotAttention(
                num_slots=self.num_slots,
                input_dim=cfg["model"]["input_dim"],
                slot_dim=cfg["model"]["slot_dim"],
                hidden_dim=cfg["model"]["hidden_dim"],
                iters=self.num_iterations)
        elif model_type == 'tsa':
            # We set the same number of inside parameters
            self.slot_attention = TransformerSlotAttention(
                num_slots=self.num_slots,
                input_dim=cfg["model"]["input_dim"],
                slot_dim=cfg["model"]["slot_dim"],
                hidden_dim=cfg["model"]["hidden_dim"],
                depth=self.num_iterations) # in a way, the depth of the transformer corresponds to the number of iterations in the original model

    def forward(self, image):
        return self.one_step(image)
    
    def configure_optimizers(self) -> Any:
        def lr_func(it):
            if it < self.peak_it:  # Warmup period
                return self.peak_lr * (it / self.peak_it)
            it_since_peak = it - self.peak_it
            return self.peak_lr * (self.decay_rate ** (it_since_peak / self.decay_it))
        optimizer = optim.Adam(self.parameters(), lr=0)
        scheduler = LambdaLR(optimizer, lr_lambda=lr_func)
        return {
            'optimizer': optimizer, 
            'lr_scheduler': {
                'scheduler': scheduler, 
                'interval': 'step' # Update the scheduler at each step
            }
        }
    
    def one_step(self, image):
        x = self.encoder(image)
        attn_shape = x.shape[-3:-1]
        slots, attn_logits, attn_slotwise = self.slot_attention(x.flatten(start_dim = 1, end_dim = 2))
        x = slots.reshape(-1, 1, 1, slots.shape[-1]).expand(-1, *self.decoder.decoder_initial_size, -1)
        x = self.decoder(x)
        
        x = F.interpolate(x, image.shape[-2:], mode='bilinear')

        x = x.unflatten(0, (len(image), len(x) // len(image)))

        recons, masks = x.split((3, 1), dim = 2)
        masks = masks.softmax(dim = 1)
        recon_combined = (recons * masks).sum(dim = 1)        
        
        return recon_combined, recons, masks, slots, attn_slotwise.unsqueeze(-2).unflatten(-1, attn_shape) if attn_slotwise is not None else None

    def training_step(self, batch, batch_idx):
        """Perform a single training step."""
        input_image = torch.squeeze(batch.get('input_images'), dim=1) # Delete dim 1 if only one view
        true_masks = torch.squeeze(batch.get('target_masks'), dim=1)

        # Get the prediction of the model and compute the loss.
        preds = self.one_step(input_image)
        recon_combined, recons, masks, slots, _ = preds
        loss_value = self.criterion(recon_combined, input_image)
        del recons, slots  # Unused.
        masks = masks.squeeze(2)
        B, num_ob, H, W = masks.shape
        masks = torch.reshape(masks, (B, num_ob, H*W))
        fg_ari = compute_adjusted_rand_index(true_masks.transpose(1, 2)[:, 1:],
                                                            masks).mean()
        del masks
        psnr = mse2psnr(loss_value)

        self.log('train_mse', loss_value, on_epoch=True)
        self.log('train_fg_ari', fg_ari, on_epoch=True)
        self.log('train_psnr', psnr, on_epoch=True)

        #self.print(f"Training metrics, MSE: {loss_value}, FG-ARI: {fg_ari}, PSNR: {psnr.item()}")
        return {'loss': loss_value}
    
    def validation_step(self, batch, batch_idx):
        """Perform a single eval step."""
        input_image = torch.squeeze(batch.get('input_images'), dim=1)
        true_masks = torch.squeeze(batch.get('target_masks'), dim=1) 

        # Get the prediction of the model and compute the loss.
        preds = self.one_step(input_image)
        recon_combined, recons, masks, slots, _ = preds
        loss_value = self.criterion(recon_combined, input_image)
        del recons, slots  # Unused.
        masks = masks.squeeze(2)
        B, num_ob, H, W = masks.shape
        masks = torch.reshape(masks, (B, num_ob, H*W))
        fg_ari = compute_adjusted_rand_index(true_masks.transpose(1, 2)[:, 1:],
                                                            masks).mean()
        del masks
        psnr = mse2psnr(loss_value)
        self.log('val_mse', loss_value, on_epoch=True)
        self.log('val_fg_ari', fg_ari, on_epoch=True)
        self.log('val_psnr', psnr, on_epoch=True)

        #self.print(f"Validation metrics, MSE: {loss_value}, FG-ARI: {fg_ari}, PSNR: {psnr.item()}")
        return {'loss': loss_value, 'val_psnr': psnr.item()}

    def test_step(self, batch, batch_idx):
        """Perform a single eval step."""
        input_image = torch.squeeze(batch.get('input_images'), dim=1)
        true_masks = torch.squeeze(batch.get('target_masks'), dim=1) 

        # Get the prediction of the model and compute the loss.
        preds = self.one_step(input_image)
        recon_combined, recons, masks, slots, _ = preds
        loss_value = self.criterion(recon_combined, input_image)
        del recons, slots  # Unused.
        masks = masks.squeeze(2)
        B, num_ob, H, W = masks.shape
        masks = torch.reshape(masks, (B, num_ob, H*W))
        fg_ari = compute_adjusted_rand_index(true_masks.transpose(1, 2)[:, 1:],
                                                            masks).mean()
        del masks
        psnr = mse2psnr(loss_value)
        self.log('test_loss', loss_value)
        self.log('test_fg_ari', fg_ari)
        self.log('test_psnr', psnr)

        #self.print(f"Test metrics, MSE: {loss_value}, FG-ARI: {fg_ari}, PSNR: {psnr.item()}")
        return {'loss': loss_value, 'test_psnr': psnr.item()}
    