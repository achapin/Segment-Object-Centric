"""
Code modified from MedSAM repository :
https://github.com/bowang-lab/MedSAM/blob/main/utils/precompute_img_embed.py

"""

import numpy as np
import os
join = os.path.join 
from tqdm import tqdm
import torch
from segment_anything import sam_model_registry
from segment_anything.utils.transforms import ResizeLongestSide
import argparse
import cv2


parser = argparse.ArgumentParser(
        description='Extract image embeddings from SAM model'
)
parser.add_argument('-i', '--img_path', type=str, default='', help='Path to the folder containing images')
parser.add_argument('-o', '--save_path', type=str, default='', help='Path to the folder containing the final embeddings')
parser.add_argument('--model_type', type=str, default='vit_h', help='model type')
parser.add_argument('--path_model', type=str, default='.', help='path to the pre-trained SAM model')
args = parser.parse_args()


model_type = args.model
if args.model == 'vit_h':
    checkpoint = args.path_model + '/sam_vit_h_4b8939.pth'
elif args.model == 'vit_b':
    checkpoint = args.path_model + '/sam_vit_b_01ec64.pth'
else:
    model_type = 'vit_l'
    checkpoint = args.path_model + '/sam_vit_l_0b3195.pth'

img_path = args.img_path 
img_files= sorted(os.listdir(img_path))

sam_model = sam_model_registry[args.model_type](checkpoint=args.checkpoint).to('cuda:0')
sam_transform = ResizeLongestSide(sam_model.image_encoder.img_size)

# compute image embeddings
images= []
for name in tqdm(img_files):
    img = cv2.imread(name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img_np = np.array(img)

    resize_img = sam_transform.apply_image(img)
    resize_img_tensor = torch.as_tensor(resize_img.transpose(2, 0, 1)).to('cuda:0')

    # model input: (1, 3, 1024, 1024)
    input_image = sam_model.preprocess(resize_img_tensor[None,:,:,:]) # (1, 3, 1024, 1024)
    assert input_image.shape == (1, 3, sam_model.image_encoder.img_size, sam_model.image_encoder.img_size), 'input image should be resized to 1024*1024'
    with torch.no_grad():
        embedding = sam_model.image_encoder(input_image)
        
    # save as npy
    np.save(join(args.save_path, name.split('.')[0]+'.npy'), embedding.cpu().numpy()[0])