import numpy as np

import os

from osrt import data


def get_dataset(mode, cfg, max_len=None, full_scale=False):
    ''' Returns a dataset.
    Args:
        mode: Dataset split, 'train', 'val', or 'test'
        cfg (dict): data config dictionary
        max_len (int): Limit to number of items in the dataset
        full_scale: Provide full images as targets, instead of a sampled set of pixels
    '''
    dataset_type = cfg['dataset']
    canonical_view = False  # OSRT uses absolute world coordinates

    dataset_folder = f'data/{dataset_type}'
    if 'path' in cfg:
        dataset_folder = cfg['path']

    points_per_item = cfg['num_points'] if 'num_points' in cfg else 2048

    if 'kwargs' in cfg:
        kwargs = cfg['kwargs']
        print(kwargs)
    else:
        kwargs = dict()

    # Create dataset
    if dataset_type == 'nmr':
        dataset = data.NMRDataset(dataset_folder, mode, points_per_item=points_per_item,
                                  max_len=max_len, full_scale=full_scale,
                                  canonical_view=canonical_view, **kwargs)
    elif dataset_type == 'msn':
        dataset = data.MultishapenetDataset(dataset_folder, mode, points_per_item=points_per_item,
                                            full_scale=full_scale, canonical_view=canonical_view, **kwargs)
    elif dataset_type == 'osrt':
        dataset = data.MultishapenetDataset(dataset_folder, mode, points_per_item=points_per_item,
                                            full_scale=full_scale, canonical_view=canonical_view,
                                            osrt=True, **kwargs)
    elif dataset_type == 'clevr3d':
        dataset = data.Clevr3dDataset(dataset_folder, mode, points_per_item=points_per_item,
                                      shapenet=False, max_len=max_len, full_scale=full_scale,
                                      canonical_view=canonical_view, **kwargs)
    elif dataset_type == 'clevr2d':
        dataset = data.Clevr2dDataset(dataset_folder, mode, **kwargs)
    elif dataset_type == 'ycb2d':
        dataset = data.YCBVideo2D(dataset_folder, mode, **kwargs)
    elif dataset_type == 'obsurf_msn':
        dataset = data.Clevr3dDataset(dataset_folder, mode, points_per_item=points_per_item,
                                      shapenet=True, max_len=max_len, full_scale=full_scale,
                                      canonical_view=canonical_view, **kwargs)
    else:
        raise ValueError('Invalid dataset "{}"'.format(cfg['dataset']))

    return dataset


def worker_init_fn(worker_id):
    ''' Worker init function to ensure true randomness.
    '''
    random_data = os.urandom(4)
    base_seed = int.from_bytes(random_data, byteorder="big")
    np.random.seed(base_seed + worker_id)


def downsample(x, num_steps=1):
    if num_steps is None or num_steps < 1:
        return x
    stride = 2**num_steps
    return x[stride//2::stride, stride//2::stride]

def crop_center(image, crop_size=192):
    height, width = image.shape[:2]

    center_x = width // 2
    center_y = height // 2

    crop_size_half = crop_size // 2

    # Calculate the top-left corner coordinates of the crop
    crop_x1 = center_x - crop_size_half
    crop_y1 = center_y - crop_size_half

    # Calculate the bottom-right corner coordinates of the crop
    crop_x2 = center_x + crop_size_half
    crop_y2 = center_y + crop_size_half

    # Crop the image
    return image[crop_y1:crop_y2, crop_x1:crop_x2]