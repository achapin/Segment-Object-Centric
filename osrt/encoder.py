import numpy as np
import torch
import torch.nn as nn
from typing import Any, Dict, List, Optional, Tuple
from torch.nn import functional as F
import math
from torchvision.ops.boxes import batched_nms
import torchvision.transforms.functional as func
from torchvision.transforms.functional import resize, to_pil_image 
from typing import Any, Dict, List, Optional, Tuple
import copy
from torchvision import transforms

from osrt.layers import RayEncoder, Transformer, SlotAttention
from osrt.utils.common import batch_iterator, MaskData, calculate_stability_score, get_indices_sorted_pos, get_positional_embedding, create_points_grid, rotation_matrix_to_euler_angles
from osrt.utils.nerf import get_extrinsic

from segment_anything import sam_model_registry
from segment_anything.modeling.image_encoder import ImageEncoderViT
from segment_anything.modeling.mask_decoder import MaskDecoder
from segment_anything.modeling.prompt_encoder import PromptEncoder
from segment_anything.utils.transforms import ResizeLongestSide
from segment_anything.utils.amg import batched_mask_to_box, remove_small_regions, mask_to_rle_pytorch, area_from_rle

from torch.nn.utils.rnn import pad_sequence

class SRTConvBlock(nn.Module):
    def __init__(self, idim, hdim=None, odim=None):
        super().__init__()
        if hdim is None:
            hdim = idim

        if odim is None:
            odim = 2 * hdim

        conv_kwargs = {'bias': False, 'kernel_size': 3, 'padding': 1}
        self.layers = nn.Sequential(
            nn.Conv2d(idim, hdim, stride=1, **conv_kwargs),
            nn.ReLU(),
            nn.Conv2d(hdim, odim, stride=2, **conv_kwargs),
            nn.ReLU())

    def forward(self, x):
        return self.layers(x)


class ImprovedSRTEncoder(nn.Module):
    """
    Scene Representation Transformer Encoder with the improvements from Appendix A.4 in the OSRT paper.
    """
    def __init__(self, num_conv_blocks=3, num_att_blocks=5, pos_start_octave=0):
        super().__init__()
        self.ray_encoder = RayEncoder(pos_octaves=15, pos_start_octave=pos_start_octave,
                                      ray_octaves=15)

        conv_blocks = [SRTConvBlock(idim=183, hdim=96)]
        cur_hdim = 192
        for i in range(1, num_conv_blocks):
            conv_blocks.append(SRTConvBlock(idim=cur_hdim, odim=None))
            cur_hdim *= 2

        self.conv_blocks = nn.Sequential(*conv_blocks)

        self.per_patch_linear = nn.Conv2d(cur_hdim, 768, kernel_size=1)

        self.transformer = Transformer(768, depth=num_att_blocks, heads=12, dim_head=64,
                                       mlp_dim=1536, selfatt=True)

    def forward(self, images, camera_pos, rays):
        """
        Args:
            images: [batch_size, num_images, 3, height, width]. Assume the first image is canonical.
            camera_pos: [batch_size, num_images, 3]
            rays: [batch_size, num_images, height, width, 3]
        Returns:
            scene representation: [batch_size, num_patches, channels_per_patch]
        """

        batch_size, num_images = images.shape[:2]

        x = images.flatten(0, 1)
        camera_pos = camera_pos.flatten(0, 1)
        rays = rays.flatten(0, 1)

        ray_enc = self.ray_encoder(camera_pos, rays)
        x = torch.cat((x, ray_enc), 1)
        x = self.conv_blocks(x)
        x = self.per_patch_linear(x)
        x = x.flatten(2, 3).permute(0, 2, 1)

        patches_per_image, channels_per_patch = x.shape[1:]
        x = x.reshape(batch_size, num_images * patches_per_image, channels_per_patch)

        x = self.transformer(x)

        return x


class OSRTEncoder(nn.Module):
    def __init__(self, pos_start_octave=0, num_slots=6, slot_dim=1536, slot_iters=1,
                 randomize_initial_slots=False):
        super().__init__()
        self.srt_encoder = ImprovedSRTEncoder(num_conv_blocks=3, num_att_blocks=5,
                                             pos_start_octave=pos_start_octave)

        self.slot_attention = SlotAttention(num_slots, slot_dim=slot_dim, iters=slot_iters,
                                            randomize_initial_slots=randomize_initial_slots)

    def forward(self, images, camera_pos, rays):
        set_latents = self.srt_encoder(images, camera_pos, rays)
        slot_latents = self.slot_attention(set_latents)
        return slot_latents


class FeatureMasking(nn.Module):
    def __init__(self, 
                 points_per_side=12,
                 box_nms_thresh = 0.7,
                 stability_score_thresh = 0.9,
                 pred_iou_thresh=0.88,
                 points_per_batch=64,
                 min_mask_region_area=0,
                 num_slots=32, 
                 slot_dim=1536, 
                 slot_iters=3,  
                 sam_model="vit_t", 
                 sam_path="mobile_sam.pt",
                 randomize_initial_slots=False):
        super().__init__() 

        # We first initialize the automatic mask generator from SAM
        # TODO : change the loading here !!!!
        sam = sam_model_registry[sam_model](checkpoint=sam_path) 
        
        self.mask_generator = SamAutomaticMask(copy.deepcopy(sam.image_encoder), 
                                               copy.deepcopy(sam.prompt_encoder), 
                                               copy.deepcopy(sam.mask_decoder), 
                                               box_nms_thresh=box_nms_thresh, 
                                               stability_score_thresh = stability_score_thresh,
                                               pred_iou_thresh=pred_iou_thresh,
                                               points_per_side=points_per_side,
                                               points_per_batch=points_per_batch,
                                               min_mask_region_area=min_mask_region_area)
        del sam
        
        self.slot_attention = SlotAttention(num_slots, input_dim=self.mask_generator.token_dim, slot_dim=slot_dim, iters=slot_iters,
                                            randomize_initial_slots=randomize_initial_slots)

    def forward(self, images, original_size, camera_pos=None, rays=None, extract_masks=False):
        """
        Args:
            images: [batch_size, num_images, height, width, 3]. Assume the first image is canonical.
            original_size: tuple(height, width) The original size of the image before transformation.
            camera_pos: [batch_size, num_images, 3]
            rays: [batch_size, num_images, height, width, 3]
        Returns:
            annotations: [batch_size, num_image] An array containing a dict for each image in each batch
                with the following annotations :
                    segmentation
                    area
                    predicted_iou
                    point_coords
                    stability_score
                    embeddings (Optionnal)
        """
        
        # Generate images
        masks = self.mask_generator(images, original_size, camera_pos, rays, extract_embeddings=True) # [B, N]
        
        B, N = masks.shape
        dim = masks[0][0]["embeddings"][0].shape[0]

        # We infer each batch separetely as it handle a different number of slots
        set_latents = None
        # TODO : set the number of slots according to either we want min or max
        with torch.no_grad():
            #num_slots = 100000
            embedding_batch = []
            masks_batch = []
            for b in range(B):
                latents_batch = torch.empty((0, dim), device=self.mask_generator.device)
                for n in range(N):
                    embeds = masks[b][n]["embeddings"]
                    #num_slots = min(len(embeds), num_slots)
                    for embed in embeds:
                        latents_batch = torch.cat((latents_batch, embed.unsqueeze(0)), 0)
                masks_batch.append(torch.zeros(latents_batch.shape[:1]))
                embedding_batch.append(latents_batch)
            set_latents = pad_sequence(embedding_batch, batch_first=True, padding_value=0.0)
            attention_mask = pad_sequence(masks_batch, batch_first=True, padding_value=1.0)
        
        # [batch_size, num_inputs = num_mask_embed x num_im, dim] 
        #self.slot_attention.change_slots_number(num_slots)
        slot_latents = self.slot_attention(set_latents, attention_mask)
        
        if extract_masks:
            return masks, slot_latents
        else:
            del masks
            return slot_latents


class SamAutomaticMask(nn.Module):
    mask_threshold: float = 0.0

    def __init__(
        self,
        image_encoder: ImageEncoderViT,
        prompt_encoder: PromptEncoder,
        mask_decoder: MaskDecoder,
        pixel_mean: List[float] = [123.675, 116.28, 103.53],
        pixel_std: List[float] = [58.395, 57.12, 57.375],
        token_dim = 1280,
        points_per_side: Optional[int] = 0,
        points_per_batch: int = 64,
        pred_iou_thresh: float = 0.88,
        stability_score_thresh: float = 0.95,
        stability_score_offset: float = 1.0,
        box_nms_thresh: float = 0.7,
        min_mask_region_area: int = 0, 
        pos_start_octave=0,
        patch_size = 16
    ) -> None:
        """
        This class adapts SAM implementation from original repository but adapting it to our needs :
        - Training only the MaskDecoder
        - Performing automatic Mask Discovery (combined with AutomaticMask from original repo)

        SAM predicts object masks from an image and input prompts.

        Arguments:
          image_encoder (ImageEncoderViT): The backbone used to encode the
            image into image embeddings that allow for efficient mask prediction.
          prompt_encoder (PromptEncoder): Encodes various types of input prompts.
          mask_decoder (MaskDecoder): Predicts masks from the image embeddings
            and encoded prompts.
          pixel_mean (list(float)): Mean values for normalizing pixels in the input image.
          pixel_std (list(float)): Std values for normalizing pixels in the input image.
        """
        super().__init__()

        # SAM part
        self.image_encoder = image_encoder
        self.prompt_encoder = prompt_encoder

        # Freeze the image encoder and prompt encoder
        for param in self.image_encoder.parameters():
            param.requires_grad = False
        for param in self.prompt_encoder.parameters():
            param.requires_grad = False

        self.mask_decoder = mask_decoder
        for param in self.mask_decoder.parameters():
            param.requires_grad = True

        # Transform image to a square by putting it to the longest side
        #self.resize = transforms.Resize(self.image_encoder.img_size, interpolation=transforms.InterpolationMode.BILINEAR)
        self.transform = ResizeLongestSide(self.image_encoder.img_size)
        
        if points_per_side > 0:
            self.points_grid = create_points_grid(points_per_side)
        else:
            self.points_grid = create_points_grid(32)
        self.points_per_batch = points_per_batch
        self.pred_iou_thresh = pred_iou_thresh
        self.stability_score_thresh = stability_score_thresh
        self.stability_score_offset = stability_score_offset
        self.box_nms_thresh = box_nms_thresh
        self.min_mask_region_area = min_mask_region_area

        self.patch_embed_dim = (self.image_encoder.img_size // patch_size)**2
        self.token_dim = token_dim
        self.tokenizer = nn.Sequential(
                nn.Linear(self.patch_embed_dim, 3072),
                nn.ReLU(),
                nn.Linear(3072, 2500),
                nn.ReLU(),
                nn.Linear(2500, self.token_dim),
            )

        self.register_buffer("pixel_mean", torch.Tensor(pixel_mean).view(-1, 1, 1), False)
        self.register_buffer("pixel_std", torch.Tensor(pixel_std).view(-1, 1, 1), False)

        # Space positional embedding
        self.ray_encoder = RayEncoder(pos_octaves=15, pos_start_octave=pos_start_octave,
                                      ray_octaves=15)

    @property
    def device(self) -> Any:
        return self.pixel_mean.device

    def forward(
        self,
        images,
        orig_size,
        camera_pos=None,
        rays=None,
        extract_embeddings = False):
        """
        Args:
            images: [batch_size, num_images, 3, height, width]. Assume the first image is canonical.
            original_size: tuple(height, width) The original size of the image before transformation.
            camera_pos: [batch_size, num_images, 3]
            rays: [batch_size, num_images, height, width, 3]
        Returns:
            annotations: [batch_size, num_image] An array containing a dict for each image in each batch
                with the following annotations :
                    segmentation
                    area
                    predicted_iou
                    point_coords
                    stability_score
                    embeddings (Optionnal)
        """
        B, N, H, W, C  = images.shape
        images = images.reshape(B*N, H, W, C) # [B x N, C, H, W] 
        #images = images.permute(0, 2, 3, 1)

        # Pre-process the images for the image encoder
        input_images = torch.stack([self.preprocess(self.transform.apply_image(x)) for x in images]) 
        im_size = self.transform.apply_image(images[0]).shape[-3:-1]

        points_scale = np.array(im_size)[None, ::-1]
        points_for_image = self.points_grid * points_scale

        with torch.no_grad():
            image_embeddings, embed_no_red = self.image_encoder(input_images, before_channel_reduc=True) # [B x N, C, H, W] 

        annotations = np.empty((B, N), dtype=object)
        i = 0
        for curr_embedding, curr_emb_no_red in zip(image_embeddings, embed_no_red):
            mask_data = MaskData()
            for (points,) in batch_iterator(self.points_per_batch, points_for_image):
                batch_data = self.process_batch(points, im_size, curr_embedding, orig_size)
                mask_data.cat(batch_data)
                del batch_data
            del curr_embedding

            # Remove duplicates
            keep_by_nms = batched_nms(
                mask_data["boxes"].float(),
                mask_data["iou_preds"],
                torch.zeros_like(mask_data["boxes"][:, 0]),  # categories
                iou_threshold=self.box_nms_thresh,
            )
            mask_data.filter(keep_by_nms)

            mask_data["segmentations"] = mask_data["masks"]

            # Extract mask embeddings
            if extract_embeddings:
                self.tokenize(mask_data, curr_emb_no_red, im_size, scale_box=1.5)
                # TODO : vérifier le positional encoding 3D
                #self.position_embeding_3d(mask_data["embeddings"], camera_pos[batch][num_im], rays[batch][num_im])

            mask_data.to_numpy()

            # Filter small disconnected regions and holes in masks NOT USED
            if self.min_mask_region_area > 0:
                mask_data = self.postprocess_small_regions(
                    mask_data,
                    self.min_mask_region_area,
                    self.box_nms_thresh,
                )
            
            # TODO : have a more efficient way to store the data
            # Write mask records
            if extract_embeddings:
                curr_ann = {
                    "embeddings": mask_data["embeddings"],
                    "segmentations": mask_data["segmentations"]
                }
            else :
                curr_ann = {
                    "segmentations": mask_data["segmentations"]
                }
            batch = math.floor((i / N))
            num_im = i % N
            annotations[batch][num_im] = curr_ann
            i+=1
        return annotations # [B, N, 1] : dict containing diverse annotations such as segmentation, area or also embedding 

    def postprocess_masks(
        self,
        masks: torch.Tensor,
        input_size: Tuple[int, ...],
        original_size: Tuple[int, ...],
    ) -> torch.Tensor:
        """
        Remove padding and upscale masks to the original image size.

        Arguments:
          masks (torch.Tensor): Batched masks from the mask_decoder,
            in BxCxHxW format.
          input_size (tuple(int, int)): The size of the image input to the
            model, in (H, W) format. Used to remove padding.
          original_size (tuple(int, int)): The original size of the image
            before resizing for input to the model, in (H, W) format.

        Returns:
          (torch.Tensor): Batched masks in BxCxHxW format, where (H, W)
            is given by original_size.
        """
        masks = F.interpolate(
            masks,
            (self.image_encoder.img_size, self.image_encoder.img_size),
            mode="bilinear",
            align_corners=True,
        )
        
        masks = masks[..., : input_size[0], : input_size[1]]
        masks = F.interpolate(masks, original_size, mode="bilinear", align_corners=True)
        return masks

    @staticmethod
    def postprocess_small_regions(
        mask_data: MaskData, min_area: int, nms_thresh: float
    ) -> MaskData:
        """
        Removes small disconnected regions and holes in masks, then reruns
        box NMS to remove any new duplicates.

        Edits mask_data in place.

        Requires open-cv as a dependency.
        """
        if len(mask_data["rles"]) == 0:
            return mask_data

        # Filter small disconnected regions and holes
        new_masks = []
        scores = []
        for mask in mask_data["masks"]:
            mask, changed = remove_small_regions(mask, min_area, mode="holes")
            unchanged = not changed
            mask, changed = remove_small_regions(mask, min_area, mode="islands")
            unchanged = unchanged and not changed

            new_masks.append(torch.as_tensor(mask).unsqueeze(0))
            # Give score=0 to changed masks and score=1 to unchanged masks
            # so NMS will prefer ones that didn't need postprocessing
            scores.append(float(unchanged))

        # Recalculate boxes and remove any new duplicates
        masks = torch.cat(new_masks, dim=0)
        boxes = batched_mask_to_box(masks)
        keep_by_nms = batched_nms(
            boxes.float(),
            torch.as_tensor(scores),
            torch.zeros_like(boxes[:, 0]),  # categories
            iou_threshold=nms_thresh,
        )

        # Only recalculate RLEs for masks that have changed
        for i_mask in keep_by_nms:
            if scores[i_mask] == 0.0:
                mask_torch = masks[i_mask].unsqueeze(0)
                mask_data["masks"][i_mask] = mask_torch
                mask_data["boxes"][i_mask] = boxes[i_mask]  # update res directly
        mask_data.filter(keep_by_nms)

        return mask_data

    def preprocess(self, x: torch.Tensor) -> torch.Tensor:
        """Normalize pixel values and pad to a square input."""
        # Rescale the image relative to the longest side
        # TODO : apply this preprocess to the dataset before training
        x = torch.as_tensor(x, device=self.device)
        x = x.permute(2, 0, 1).contiguous()#[None, :, :, :]
        
        # Normalize colors
        x = (x - self.pixel_mean) / self.pixel_std

        # Pad
        h, w = x.shape[-2:]
        padh = self.image_encoder.img_size - h
        padw = self.image_encoder.img_size - w
        x = F.pad(x, (0, padw, 0, padh))
        return x

    def process_batch(
        self,
        points: np.ndarray,
        im_size: Tuple[int, ...],
        curr_embedding,
        curr_orig_size
        ):

        # Run model on this batch
        in_points = torch.as_tensor(self.transform.apply_coords(points, im_size), device=self.device)
        in_labels = torch.ones(in_points.shape[0], dtype=torch.int, device=in_points.device)
        
        masks, iou_preds, _ = self.predict_masks(
            in_points[:, None, :],
            in_labels[:, None],
            curr_orig_size,
            im_size,
            curr_embedding,
            multimask_output=True,
            return_logits=True
        )

        data = MaskData(
            masks=masks.flatten(0, 1),
            iou_preds=iou_preds.flatten(0, 1),
            points=torch.as_tensor(points.repeat(masks.shape[1], axis=0)),
        )
        del masks

        # Filter by predicted IoU
        if self.pred_iou_thresh > 0.0:
            keep_mask = data["iou_preds"] > self.pred_iou_thresh
            data.filter(keep_mask)

        # Calculate stability score
        data["stability_score"] = calculate_stability_score(
            data["masks"], self.mask_threshold, self.stability_score_offset
        )
        if self.stability_score_thresh > 0.0:
            keep_mask = data["stability_score"] >= self.stability_score_thresh
            data.filter(keep_mask)

        # Threshold masks and calculate boxes
        data["masks"] = data["masks"] > self.mask_threshold
        data["boxes"] = batched_mask_to_box(data["masks"])

        data["rles"] = mask_to_rle_pytorch(data["masks"])

        return data

    def predict_masks(
        self,
        point_coords: Optional[torch.Tensor],
        point_labels: Optional[torch.Tensor],
        curr_orig_size,
        curr_input_size,
        curr_embedding,
        boxes: Optional[torch.Tensor] = None,
        mask_input: Optional[torch.Tensor] = None,
        multimask_output: bool = True,
        return_logits: bool = False,
    ) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        """
        Predict masks for the given input prompts, using the current image.
        Input prompts are batched torch tensors and are expected to already be
        transformed to the input frame using ResizeLongestSide.

        Arguments:
          point_coords (torch.Tensor or None): A BxNx2 array of point prompts to the
            model. Each point is in (X,Y) in pixels.
          point_labels (torch.Tensor or None): A BxN array of labels for the
            point prompts. 1 indicates a foreground point and 0 indicates a
            background point.
          boxes (np.ndarray or None): A Bx4 array given a box prompt to the
            model, in XYXY format.
          mask_input (np.ndarray): A low resolution mask input to the model, typically
            coming from a previous prediction iteration. Has form Bx1xHxW, where
            for SAM, H=W=256. Masks returned by a previous iteration of the
            predict method do not need further transformation.
          multimask_output (bool): If true, the model will return three masks.
            For ambiguous input prompts (such as a single click), this will often
            produce better masks than a single prediction. If only a single
            mask is needed, the model's predicted quality score can be used
            to select the best mask. For non-ambiguous prompts, such as multiple
            input prompts, multimask_output=False can give better results.
          return_logits (bool): If true, returns un-thresholded masks logits
            instead of a binary mask.

        Returns:
          (torch.Tensor): The output masks in BxCxHxW format, where C is the
            number of masks, and (H, W) is the original image size.
          (torch.Tensor): An array of shape BxC containing the model's
            predictions for the quality of each mask.
          (torch.Tensor): An array of shape BxCxHxW, where C is the number
            of masks and H=W=256. These low res logits can be passed to
            a subsequent iteration as mask input.
        """
        if point_coords is not None:
            points = (point_coords, point_labels)
        else:
            points = None
        # Embed prompts
        with torch.no_grad():
            sparse_embeddings, dense_embeddings = self.prompt_encoder(
                points=points,
                boxes=boxes,
                masks=mask_input,
            )
        # Predict masks
        low_res_masks, iou_predictions = self.mask_decoder(
            image_embeddings=curr_embedding.unsqueeze(0),
            image_pe=self.prompt_encoder.get_dense_pe(),
            sparse_prompt_embeddings=sparse_embeddings,
            dense_prompt_embeddings=dense_embeddings,
            multimask_output=multimask_output,
        )

        # Upscale the masks to the original image resolution
        masks = self.postprocess_masks(low_res_masks, curr_input_size, curr_orig_size)

        if not return_logits:
            masks = masks > self.mask_threshold

        return masks, iou_predictions.detach(), low_res_masks.detach()

    def tokenize(self, mask_data, image_embed, input_size, scale_box=1.5):
        """
        Predicts the embeddings from each mask given the global embedding and
        a scale factor around each mask.

        Arguments:
          mask_data : the data of the masks extracted by SAM
          image_embed : embedding of the corresponding image
          scale_box : factor by which the bounding box will be scaled

        Returns:
          embeddings : the embeddings for each mask extracted from the image
        """
        image_embed = image_embed.permute(2, 0, 1)
        mean_embed = image_embed.mean(dim=0) # get the mean value of image embedding

        orig_H, orig_W = mask_data["segmentations"][0].shape[:2]
        out_size = mean_embed.shape

        # Put the mean image embedding back to the input format 
        scaled_img_emb = self.postprocess_masks(mean_embed.unsqueeze(0).unsqueeze(0), input_size, (orig_H, orig_W)).squeeze()

        mask_data["embeddings"] = []

        # Sort the data of the mask by their 2D position
        indices = get_indices_sorted_pos(mask_data)
        mask_data.sort_by_indices(indices)

        for idx in range(len(mask_data["segmentations"])):
            mask = mask_data["segmentations"][idx]
            
            mask_embed = (scaled_img_emb * mask).unsqueeze(0)
            mask_embed = func.resize(
                mask_embed,
                out_size,
                interpolation=func.InterpolationMode.BILINEAR,
            ).squeeze()

            mask_embed = mask_embed.flatten()
            token_embed = self.tokenizer(mask_embed).detach().cpu().numpy()
            pos_embedding = get_positional_embedding(idx, self.token_dim)
            token_embed += pos_embedding

            # Apply mask to image embedding
            mask_data["embeddings"].append(torch.from_numpy(token_embed).to(self.device)) # [token_dim] 

    def position_embeding_3d(self, tokens, camera_pos, rays):
        """
        Args:
            tokens: list of tokens to embedd
            camera_pos: [3]
            rays: [height, width, 3]
        """
        extrinsic = get_extrinsic(camera_pos, rays)
        x, y ,z = extrinsic[0][-1], extrinsic[1][-1], extrinsic[2][-1]
        rot_x, rot_y, rot_z = rotation_matrix_to_euler_angles(extrinsic[0:3, 0:3])

        pos_x, pox_y, pos_z = get_positional_embedding(x, self.token_dim), get_positional_embedding(y, self.token_dim), get_positional_embedding(z, self.token_dim)
        pos_rot_x, pos_rot_y, pos_rot_z = get_positional_embedding(rot_x, self.token_dim), get_positional_embedding(rot_y, self.token_dim), get_positional_embedding(rot_z, self.token_dim)

        pos_embed = (pos_rot_x + pos_rot_y + pos_rot_z + pos_x + pox_y + pos_z) // 6
        tokens += pos_embed