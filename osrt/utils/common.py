import math
import os

import torch
import torch.distributed as dist

from typing import Any, List, Generator, ItemsView
import numpy as np
import math
from copy import deepcopy

__LOG10 = math.log(10)

# Method extracted from SAM repo in segment_anything.utils.amg.py
def batch_iterator(batch_size: int, *args) -> Generator[List[Any], None, None]:
    assert len(args) > 0 and all(
        len(a) == len(args[0]) for a in args
    ), "Batched iteration must have inputs of all the same size."
    n_batches = len(args[0]) // batch_size + int(len(args[0]) % batch_size != 0)
    for b in range(n_batches):
        yield [arg[b * batch_size : (b + 1) * batch_size] for arg in args]

# Method extracted from SAM repo in segment_anything.utils.amg.py
class MaskData:
    """
    A structure for storing masks and their related data in batched format.
    Implements basic filtering and concatenation.
    """

    def __init__(self, **kwargs) -> None:
        for v in kwargs.values():
            assert isinstance(
                v, (list, np.ndarray, torch.Tensor)
            ), "MaskData only supports list, numpy arrays, and torch tensors."
        self._stats = dict(**kwargs)

    def __setitem__(self, key: str, item: Any) -> None:
        assert isinstance(
            item, (list, np.ndarray, torch.Tensor)
        ), "MaskData only supports list, numpy arrays, and torch tensors."
        self._stats[key] = item

    def __delitem__(self, key: str) -> None:
        del self._stats[key]

    def __getitem__(self, key: str) -> Any:
        return self._stats[key]

    def items(self) -> ItemsView[str, Any]:
        return self._stats.items()

    def filter(self, keep: torch.Tensor) -> None:
        for k, v in self._stats.items():
            if v is None:
                self._stats[k] = None
            elif isinstance(v, torch.Tensor):
                self._stats[k] = v[torch.as_tensor(keep, device=v.device)]
            elif isinstance(v, np.ndarray):
                self._stats[k] = v[keep.detach().cpu().numpy()]
            elif isinstance(v, list) and keep.dtype == torch.bool:
                self._stats[k] = [a for i, a in enumerate(v) if keep[i]]
            elif isinstance(v, list):
                self._stats[k] = [v[i] for i in keep]
            else:
                raise TypeError(f"MaskData key {k} has an unsupported type {type(v)}.")

    def sort_by_indices(self, indices) -> None:
        """
        Sort the elements of the MaskData by a list of indices 
        """
        for k, v in self._stats.items():
            if len(v) > 0:
                if v is None:
                    self._stats[k] = None
                elif isinstance(v, torch.Tensor):
                    self._stats[k] = v[indices]
                elif isinstance(v, np.ndarray):
                    self._stats[k] = v[indices]
                elif isinstance(v, list):
                    self._stats[k] = [v[i] for i in indices]
                else:
                    raise TypeError(f"MaskData key {k} has an unsupported type {type(v)}.")

    def cat(self, new_stats: "MaskData") -> None:
        for k, v in new_stats.items():
            if k not in self._stats or self._stats[k] is None:
                self._stats[k] = deepcopy(v)
            elif isinstance(v, torch.Tensor):
                self._stats[k] = torch.cat([self._stats[k], v], dim=0)
            elif isinstance(v, np.ndarray):
                self._stats[k] = np.concatenate([self._stats[k], v], axis=0)
            elif isinstance(v, list):
                self._stats[k] = self._stats[k] + deepcopy(v)
            else:
                raise TypeError(f"MaskData key {k} has an unsupported type {type(v)}.")

    def to_numpy(self) -> None:
        for k, v in self._stats.items():
            if isinstance(v, torch.Tensor):
                self._stats[k] = v.detach().cpu().numpy()

def create_points_grid(number_points):
    """Generates a 2D grid of points evenly spaced in [0,1]x[0,1]."""
    offset = 1 / (2 * number_points)
    points_one_side = np.linspace(offset, 1 - offset, number_points)
    points_x = np.tile(points_one_side[None, :], (number_points, 1))
    points_y = np.tile(points_one_side[:, None], (1, number_points))
    points = np.stack([points_x, points_y], axis=-1).reshape(-1, 2)
    return points

def get_positional_embedding(position, token_dim):
    position_encodings = np.zeros(token_dim)
    div_term = np.exp(np.arange(0, token_dim, 2).astype(np.float32) * (-math.log(10000.0) / token_dim))
    position_encodings[0::2] = np.sin(position * div_term)
    position_encodings[1::2] = np.cos(position * div_term)
    return position_encodings
    
def rotation_matrix_to_euler_angles(R) :
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
 
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

def get_indices_sorted_pos(mask_data: MaskData) -> List[int]:
    """
    Return the indices of the boxes sorted by their 2D position on the image 
    """
    boxes = mask_data["boxes"].cpu().numpy()
    centers = []
    i = 0
    for box in boxes:
        center_x = (box[2] - box[0])/2 + box[0]
        center_y = (box[3] - box[1])/2 + box[1]
        centers.append((center_x, center_y, i))
        i+=1
    sorted_centers = sorted(centers , key=lambda p: [p[1], p[0], p[2]])
    return [cent[2] for cent in sorted_centers]


# Method extracted from SAM repo in segment_anything.utils.amg.py
def calculate_stability_score(
    masks: torch.Tensor, mask_threshold: float, threshold_offset: float
) -> torch.Tensor:
    """
    Computes the stability score for a batch of masks. The stability
    score is the IoU between the binary masks obtained by thresholding
    the predicted mask logits at high and low values.
    """
    # One mask is always contained inside the other.
    # Save memory by preventing unnecessary cast to torch.int64
    intersections = (
        (masks > (mask_threshold + threshold_offset))
        .sum(-1, dtype=torch.int16)
        .sum(-1, dtype=torch.int32)
    )
    unions = (
        (masks > (mask_threshold - threshold_offset))
        .sum(-1, dtype=torch.int16)
        .sum(-1, dtype=torch.int32)
    )
    return intersections / unions

def mse2psnr(x):
    return -10.*torch.log(x)/__LOG10


def init_ddp():
    try:
        local_rank = int(os.environ["LOCAL_RANK"])
        world_size = int(os.environ["WORLD_SIZE"])
        print(f"World size (nb GPUs x nb nodes) : {world_size}")
    except KeyError:
        return 0, 1  # Single GPU run
        
    dist.init_process_group(backend="nccl")
    print(f'Initialized process {local_rank} / {world_size}')
    torch.cuda.set_device(local_rank)

    setup_dist_print(local_rank == 0)
    return local_rank, world_size

def cleanup():
    dist.destroy_process_group()

def setup_dist_print(is_main):
    import builtins as __builtin__
    builtin_print = __builtin__.print

    def print(*args, **kwargs):
        force = kwargs.pop('force', False)
        if is_main or force:
            builtin_print(*args, **kwargs)

    __builtin__.print = print


def using_dist():
    return dist.is_available() and dist.is_initialized()


def get_world_size():
    if not using_dist():
        return 1
    return dist.get_world_size()


def get_rank():
    if not using_dist():
        return 0
    return dist.get_rank()


def gather_all(tensor):
    world_size = get_world_size()
    if world_size < 2:
        return [tensor]

    tensor_list = [torch.zeros_like(tensor) for _ in range(world_size)]
    dist.all_gather(tensor_list, tensor)

    return tensor_list


def reduce_dict(input_dict, average=True):
    """
    Reduces the values in input_dict across processes, when distributed computation is used.
    In all processes, the dicts should have the same keys mapping to tensors of identical shape.
    """
    world_size = get_world_size()
    if world_size < 2:
        return input_dict

    keys = sorted(input_dict.keys())
    values = [input_dict[k] for k in keys] 

    if average:
        op = dist.ReduceOp.AVG
    else:
        op = dist.ReduceOp.SUM

    for value in values:
        dist.all_reduce(value, op=op)

    reduced_dict = {k: v for k, v in zip(keys, values)}

    return reduced_dict


def compute_adjusted_rand_index(true_mask, pred_mask):
    """
    Computes the adjusted rand index (ARI) of a given image segmentation, ignoring the background.
    Implementation following https://github.com/deepmind/multi_object_datasets/blob/master/segmentation_metrics.py#L20
    Args:
        true_mask: Tensor of shape [batch_size, n_true_groups, n_points] containing true
            one-hot coded cluster assignments, with background being indicated by zero vectors.
        pred_mask: Tensor of shape [batch_size, n_pred_groups, n_points] containing predicted
            cluster assignments encoded as categorical probabilities.
    """
    batch_size, n_true_groups, n_points = true_mask.shape
    n_pred_groups = pred_mask.shape[1]

    if n_points <= n_true_groups and n_points <= n_pred_groups:
        raise ValueError(
          "adjusted_rand_index requires n_groups < n_points. We don't handle "
          "the special cases that can occur when you have one cluster "
          "per datapoint.")

    true_group_ids = true_mask.argmax(1)
    pred_group_ids = pred_mask.argmax(1)

    # Convert to one-hot ('oh') representations
    true_mask_oh = true_mask.float()
    pred_mask_oh = torch.eye(n_pred_groups).to(pred_mask)[pred_group_ids].transpose(1, 2)

    n_points_fg = true_mask_oh.sum((1, 2))

    nij = torch.einsum('bip,bjp->bji', pred_mask_oh, true_mask_oh)

    nij = nij.double()  # Cast to double, since the expected_rindex can introduce numerical inaccuracies

    a = nij.sum(1)
    b = nij.sum(2)

    rindex = (nij * (nij - 1)).sum((1, 2))
    aindex = (a * (a - 1)).sum(1)
    bindex = (b * (b - 1)).sum(1)
    expected_rindex = aindex * bindex / (n_points_fg * (n_points_fg - 1))
    max_rindex = (aindex + bindex) / 2

    ari = (rindex - expected_rindex) / (max_rindex - expected_rindex)

    # We can get NaN in case max_rindex == expected_rindex. This happens when both true and
    # predicted segmentations consist of only a single segment. Since we are allowing the
    # true segmentation to contain zeros (i.e. background) which we ignore, it suffices
    # if the foreground pixels belong to a single segment.

    # We check for this case, and instead set the ARI to 1.

    def _fg_all_equal(values, bg):
        """
        Check if all pixels in values that do not belong to the background (bg is False) have the same
        segmentation id.
        Args:
            values: Segmentations ids given as integer Tensor of shape [batch_size, n_points]
            bg: Binary tensor indicating background, shape [batch_size, n_points]
        """
        fg_ids = (values + 1) * (1 - bg.int()) # Move fg ids to [1, n], set bg ids to 0
        example_fg_id = fg_ids.max(1, keepdim=True)[0]  # Get the id of an arbitrary fg cluster.
        return torch.logical_or(fg_ids == example_fg_id[..., :1],  # All pixels should match that id...
                                bg  # ...or belong to the background.
                               ).all(-1)

    background = (true_mask.sum(1) == 0)
    both_single_cluster = torch.logical_and(_fg_all_equal(true_group_ids, background),
                                            _fg_all_equal(pred_group_ids, background))

    # Ensure that we are only (close to) getting NaNs in exactly the case described above.
    matching = (both_single_cluster == torch.isclose(max_rindex, expected_rindex))

    if not matching.all().item():
        offending_idx = matching.int().argmin()

    return torch.where(both_single_cluster, torch.ones_like(ari), ari)


