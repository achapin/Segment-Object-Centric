import numpy as np
import os
from os import listdir, mkdir
from os.path import isfile, join, isdir
import json

def preprocess_ycb(orig_path, final_path):
    """
    Take the original YCB Video dataset obtained from https://github.com/yuxng/YCB_Video_toolbox/tree/master and process it
    to follow the clevr3d format. 

    orig_path: path from original YCB dataset
    final_path: path to the new format registered
    """
    try:
        if isdir(final_path):
            mkdir(final_path + "/depths")
            mkdir(final_path + "/images")
            mkdir(final_path + "/masks")
    except:
        print("An exception occurred while creating new folders") 
    
    path_train = join(orig_path, "/train_real")

    # Creating a symlink of images to the right folders 
    for f in listdir(path_train):
        folder_scene = join(path_train, f)
        if isdir(folder_scene):
            depth_path = join(folder_scene, "/depth")
            mask_path = join(folder_scene, "/mask")
            rgb_path = join(folder_scene, "/depth")
            for img in listdir(depth_path):
                depth_img_path = join(depth_path, img)
                mask_img_path = join(mask_path, img)
                rgb_img_path = join(rgb_path, img)
                if isfile(depth_img_path):
                    depth_dst_path = join(final_path, f"/depths/{f}-" + img)
                    mask_dst_path = join(final_path, f"/masks/{f}-" + img)
                    rgb_dst_path = join(final_path, f"/images/{f}-" + img)
                    os.symlink(depth_img_path, depth_dst_path)
                    os.symlink(mask_img_path, mask_dst_path)
                    os.symlink(rgb_img_path, rgb_dst_path)
                    #shutil.copyfile(depth_img_path, depth_dst_path)
                    #shutil.copyfile(mask_img_path, mask_dst_path)
                    #shutil.copyfile(rgb_img_path, rgb_dst_path)
            camera_poses = []
            camera_file = join(folder_scene,"/scene_camera.json")
            with open(camera_file, 'r') as f:
                data = json.load(f)
                rotation = data["cam_R_w2c"]
                translation = data["cam_t_w2c"]

