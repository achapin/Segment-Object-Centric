import datetime
import time
import torch
import torch.nn as nn
import torch.optim as optim
import argparse
import yaml

from osrt.model import LitSlotAttentionAutoEncoder
from osrt import data
from osrt.utils.visualization_utils import visualize_slot_attention
from osrt.utils.common import mse2psnr

from torch.utils.data import DataLoader
import torch.nn.functional as F
from tqdm import tqdm

# TODO : setup with lightning

def main():
    # Arguments
    parser = argparse.ArgumentParser(
        description='Train a 3D scene representation model.'
    )
    parser.add_argument('config', type=str, help="Where to save the checkpoints.")
    parser.add_argument('--wandb', action='store_true', help='Log run to Weights and Biases.')
    parser.add_argument('--seed', type=int, default=0, help='Random seed.')
    parser.add_argument('--ckpt', type=str, default=".", help='Model checkpoint path')
    parser.add_argument('--output', type=str, default="./outputs/", help='Folder in which to save images')
    parser.add_argument('--step', type=int, default=".", help='Step of the model')

    args = parser.parse_args()
    with open(args.config, 'r') as f:
        cfg = yaml.load(f, Loader=yaml.CLoader)

    ### Set random seed.
    torch.manual_seed(args.seed)

    ### Hyperparameters of the model.
    num_slots = cfg["model"]["num_slots"]
    num_iterations = cfg["model"]["iters"]
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    resolution = (128, 128)
    
    #### Create datasets
    vis_dataset = data.get_dataset('val', cfg['data'])
    vis_loader = DataLoader(
        vis_dataset, batch_size=1, num_workers=cfg["training"]["num_workers"],
        shuffle=True, worker_init_fn=data.worker_init_fn)

    #### Create model
    model = LitSlotAttentionAutoEncoder(resolution, num_slots, num_iterations, cfg=cfg).to(device)
    checkpoint = torch.load(args.ckpt)

    model.load_state_dict(checkpoint['state_dict'])

    model.eval()

    #### Visualize image
    image = torch.squeeze(next(iter(vis_loader)).get('input_images').to(device), dim=1)
    image = F.interpolate(image, size=128)
    image = image.to(device)
    recon_combined, recons, masks, _, _ = model(image)
    loss = nn.MSELoss()
    loss_value = loss(recon_combined, image)
    psnr = mse2psnr(loss_value)
    print(f"MSE {loss_value} and PSNR {psnr}")
    visualize_slot_attention(num_slots, image, recon_combined, recons, masks, folder_save=args.output, step=args.step, save_file=True)
                  
if __name__ == "__main__":
    main()